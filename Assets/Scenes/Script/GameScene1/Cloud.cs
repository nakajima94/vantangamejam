﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{
    //弾プレハブ
    [SerializeField]
    public GameObject shotPrefab_ = null;

    //エネミー座標用
    [SerializeField]
    public GameObject enemy_ = null;

    //卜ラップ座標用
    [SerializeField]
    public GameObject trap_ = null;


    //球の速度
    //[SerializeField]
    private float shot_speed_ = 2;

    [SerializeField]
    public float attack_time_ = 0;

    GameObject t;

    public IEnumerator enemyAtack()
    {

        //雲の位置
        Vector2 Cloud_pos_ = this.transform.position;
        //敵の位置
        Vector2 Enemy_pos_ = enemy_.transform.position;

        //弾のプレハブを作成
        t = Instantiate(shotPrefab_);

        //弾のプレハブの位置を敵の位置にする
        t.transform.position = Enemy_pos_;

        //敵からプレイヤーに向かうベクトルをつくる
        //プレイヤーの位置から敵の位置（弾の位置）を引く
        Vector2 vec = Cloud_pos_ - Enemy_pos_;

        var a = vec.magnitude;
        //vec.Normalize();
        Debug.Log(a);
        float dis = Vector2.Distance(Enemy_pos_, Cloud_pos_);
        shot_speed_ = dis / attack_time_;
        //弾のRigidBody2Dコンポネントのvelocityに先程求めたベクトルを入れて力を加える
        t.GetComponent<Rigidbody2D>().velocity = vec * shot_speed_;
        /*

        //上記2つの距離の差
        //float dis = Vector3.Distance(next_pos_, move_);

        Vector2 move_ = Cloud_pos_ - Enemy_pos_;
        move_.Normalize();

        Vector2 next_pos_ = Enemy_pos_ + (move_ * shot_speed_);

        */
        yield return null;
    }

    //消す処理
    public IEnumerator shotdel()
    {
        Destroy(t);
        yield return null;
    }

    public IEnumerator FadeOutObject()
    {
        while (this.GetComponent<Renderer>().material.color.a > 0)
        {
            Color objectColor = this.GetComponent<Renderer>().material.color;
            float fadeAmount = objectColor.a - (1 * Time.deltaTime);

            objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
            this.GetComponent<Renderer>().material.color = objectColor;
            if (this.GetComponent<Renderer>().material.color.a < 0)
            {
                gameObject.SetActive(false);
            }
            yield return null;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            StartCoroutine(FadeOutObject());

            //弾発射のフラグ
            StartCoroutine(enemyAtack());

        }

        if (collision.gameObject.tag == "shot")
        {
            //弾の消す処理 
            StartCoroutine(shotdel());
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}