﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    Canvas UI;

    [SerializeField]
    GameObject Clear_Text;

    [SerializeField]
    GameObject GameOver_Text;

    [SerializeField]
    private int StageNo;


    // Start is called before the first frame update
    void Start()
    {
        



    }

    // Update is called once per frame
    void Update()
    {
        




    }


    public void GameOverInvoke()
    {


        GameOver_Text.SetActive(true);
        Invoke("GameOverBack", 1f);

    }


    public void GameOverBack() {


        SceneManager.LoadScene("GameScene1");



    }


  


    public void Goal() {


        Clear_Text.SetActive(true);

        if (PlayerPrefs.GetInt("CLEAR", 0) < StageNo)
        {
            PlayerPrefs.SetInt("CLEAR", StageNo);
        }



    }


    public void GoBack() {



        SceneManager.LoadScene("StageSelectScene");
    
    
    }



}
