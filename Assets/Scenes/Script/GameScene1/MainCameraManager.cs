﻿using UnityEngine;
public class MainCameraManager : MonoBehaviour
{
    [SerializeField] GameObject Player = null;
    void Update()
    {
        if (Player)
        {
            Vector3 CamPos = new Vector3(Player.transform.position.x+3.5f, Player.transform.position.y +2f, transform.position.z);
            if (CamPos.x < 0)
            {
                CamPos.x = 0;
            }
            if (CamPos.x > 100/*マップの右端*/)
            {
                CamPos.x = 100;
            }
            transform.position = CamPos;
        }
        else
        {
            // プレイヤーが死んだら消して、ここが読まれる
            // 視点が適当な位置に移動してGAMEOVERを表示
        }
    }
}