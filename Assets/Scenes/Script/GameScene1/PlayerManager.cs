﻿using System.Collections;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private Rigidbody2D rbody;

    [SerializeField]
    private float SmallJumpPower = 400;

    [SerializeField]
    private float BigJumpPower = 200;

    public LayerMask blockLayer; //ブロックレイヤー

    [SerializeField]
    private float STEP = 7.0f;

    [SerializeField]
    AudioClip Jump_small;

    [SerializeField]
    AudioClip Jump_Big;

    public GameObject _gameManagaer;

    AudioSource audioSource;


    // Start is called before the first frame update
    void Start()
    {

        rbody = GetComponent<Rigidbody2D>();
        //_gameManagaer = GetComponent<GameObject>();

        //Componentを取得
        audioSource = GetComponent<AudioSource>();

    }


    void Update()
    {

        this.transform.position += new Vector3(STEP * Time.deltaTime, 0, 0);

        //Debug.Log(STEP * Time.deltaTime);

        if (Input.GetMouseButtonDown(0) && this.rbody.velocity.y == 0)
        {
            rbody.AddForce(Vector2.up * SmallJumpPower);
            audioSource.PlayOneShot(Jump_small);

        }

        if (Input.GetMouseButtonDown(1) && this.rbody.velocity.y == 0)
        {
            rbody.AddForce(Vector2.up * BigJumpPower);
            audioSource.PlayOneShot(Jump_Big);

        }

        //ゲームオーバー判定
        if (this.transform.position.y < -10) {


            GameOverGO();


        }

        PushJumpButton();


    }

    //マウス
    public void PushJumpButton()
    {

        if (Input.GetKeyDown(KeyCode.A) && this.rbody.velocity.y == 0)
        {
            rbody.AddForce(Vector2.up * SmallJumpPower);


        }

        if (Input.GetKeyDown(KeyCode.D) && this.rbody.velocity.y == 0)
        {
            rbody.AddForce(Vector2.up * BigJumpPower);

        }



    }

    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Trap")
        {
            
            StartCoroutine(Destroy_Corutin(0.3f, collision));
           
        }

        if (collision.gameObject.tag == "Goal")
        {

            Debug.Log("Goal");
            CLEAR();


        }


    }


    IEnumerator Destroy_Corutin(float delay,Collider2D collision) {


        yield return new WaitForSeconds(delay);
        collision.gameObject.SetActive(false);
        //yield break;

    }



    public void GameOverGO() {


        _gameManagaer.GetComponent<GameManager>().GameOverInvoke();



    }


    public void CLEAR()
    {


        _gameManagaer.GetComponent<GameManager>().Goal();



    }








}
