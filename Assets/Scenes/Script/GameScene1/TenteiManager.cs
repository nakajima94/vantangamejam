﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TenteiManager : MonoBehaviour
{
    public IEnumerator FadeOutObject()
    {
        while (this.GetComponent<Renderer>().material.color.a < 0)
        {
            Color objectColor = this.GetComponent<Renderer>().material.color;
            float fadeAmount = objectColor.a + (1 * Time.deltaTime);

            objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
            this.GetComponent<Renderer>().material.color = objectColor;
            if (this.GetComponent<Renderer>().material.color.a < 0)
            {
                gameObject.SetActive(true);
            }
            yield return null;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {

            StartCoroutine(FadeOutObject());

        }

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
