﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Next_Scene : MonoBehaviour
{
    [SerializeField]
    private string _nextScene = ""; // 次のシーンの名前入力

    private void NextScene()
    {
        Debug.Log("next");
        //var name = "Scenes/" + _nextScene;
        SceneManager.LoadScene("StageSelectScene");

    }
}
